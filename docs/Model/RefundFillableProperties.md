# # RefundFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** | статус заявки на возврат из RefundStatusEnum | [optional] 
**responsible_id** | **int** | идентификатор ответственного | [optional] 
**rejection_comment** | **string** | причина отклонения | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


