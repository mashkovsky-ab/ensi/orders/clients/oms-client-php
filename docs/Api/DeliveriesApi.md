# Ensi\OmsClient\DeliveriesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDelivery**](DeliveriesApi.md#getDelivery) | **GET** /orders/deliveries/{id} | Получение объекта типа Delivery
[**patchDelivery**](DeliveriesApi.md#patchDelivery) | **PATCH** /orders/deliveries/{id} | Обновление части полей объекта типа Delivery
[**searchDeliveries**](DeliveriesApi.md#searchDeliveries) | **POST** /orders/deliveries:search | Поиск объектов типа Delivery



## getDelivery

> \Ensi\OmsClient\Dto\DeliveryResponse getDelivery($id, $include)

Получение объекта типа Delivery

Получение объекта типа Delivery

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\DeliveriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getDelivery($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveriesApi->getDelivery: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\OmsClient\Dto\DeliveryResponse**](../Model/DeliveryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchDelivery

> \Ensi\OmsClient\Dto\DeliveryResponse patchDelivery($id, $delivery_for_patch)

Обновление части полей объекта типа Delivery

Обновление части полей объекта типа Delivery

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\DeliveriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$delivery_for_patch = new \Ensi\OmsClient\Dto\DeliveryForPatch(); // \Ensi\OmsClient\Dto\DeliveryForPatch | 

try {
    $result = $apiInstance->patchDelivery($id, $delivery_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveriesApi->patchDelivery: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **delivery_for_patch** | [**\Ensi\OmsClient\Dto\DeliveryForPatch**](../Model/DeliveryForPatch.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\DeliveryResponse**](../Model/DeliveryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchDeliveries

> \Ensi\OmsClient\Dto\SearchDeliveriesResponse searchDeliveries($search_deliveries_request)

Поиск объектов типа Delivery

Поиск объектов типа Delivery

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\DeliveriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_deliveries_request = new \Ensi\OmsClient\Dto\SearchDeliveriesRequest(); // \Ensi\OmsClient\Dto\SearchDeliveriesRequest | 

try {
    $result = $apiInstance->searchDeliveries($search_deliveries_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeliveriesApi->searchDeliveries: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_deliveries_request** | [**\Ensi\OmsClient\Dto\SearchDeliveriesRequest**](../Model/SearchDeliveriesRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\SearchDeliveriesResponse**](../Model/SearchDeliveriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

