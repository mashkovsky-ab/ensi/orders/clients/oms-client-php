# # Delivery

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID отправления | [optional] 
**order_id** | **int** | ID заказа | [optional] 
**number** | **string** | номер отправления | [optional] 
**status_at** | [**\DateTime**](\DateTime.md) | дата установки статуса | [optional] 
**cost** | **int** | себестоимость доставки, полученная от службы доставки в копейках | [optional] 
**width** | **float** | ширина | [optional] 
**height** | **float** | высота | [optional] 
**length** | **float** | длина | [optional] 
**weight** | **float** | вес | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | [optional] 
**date** | [**\DateTime**](\DateTime.md) | желаемая дата доставки | [optional] 
**timeslot** | [**\Ensi\OmsClient\Dto\Timeslot**](Timeslot.md) |  | [optional] 
**status** | **int** | статус отправления из DeliveryStatusEnum | [optional] 
**shipments** | [**\Ensi\OmsClient\Dto\Shipment[]**](Shipment.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


