# Ensi\OmsClient\OrdersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addOrderComment**](OrdersApi.md#addOrderComment) | **PUT** /orders/orders/{id}:comment | Добавить внутренний комментарий
[**attachOrderFile**](OrdersApi.md#attachOrderFile) | **POST** /orders/orders/{id}:attach-file | Прикрепить файл к заказу
[**changeOrderDelivery**](OrdersApi.md#changeOrderDelivery) | **POST** /orders/orders/{id}:change-delivery | Обновление полей по доставке объекта типа Order
[**changeOrderPaymentSystem**](OrdersApi.md#changeOrderPaymentSystem) | **POST** /orders/orders/{id}:change-payment-method | Изменить систему оплаты заказа
[**checkOrderPayment**](OrdersApi.md#checkOrderPayment) | **GET** /orders/orders/{id}:check-payment | Проверить статус оплаты заказа
[**commitOrder**](OrdersApi.md#commitOrder) | **POST** /orders/orders:commit | Оформить заказ
[**deleteOrderFiles**](OrdersApi.md#deleteOrderFiles) | **DELETE** /orders/orders/{id}:delete-files | Удалить прикрепленные файлы к заказу
[**getOrder**](OrdersApi.md#getOrder) | **GET** /orders/orders/{id} | Получение объекта типа Order
[**patchOrder**](OrdersApi.md#patchOrder) | **PATCH** /orders/orders/{id} | Обновление части полей объекта типа Order
[**searchOrders**](OrdersApi.md#searchOrders) | **POST** /orders/orders:search | Поиск объектов типа Order
[**startOrderPayment**](OrdersApi.md#startOrderPayment) | **POST** /orders/orders/{id}:start-payment | Запустить оплату



## addOrderComment

> \Ensi\OmsClient\Dto\EmptyDataResponse addOrderComment($id, $order_add_comment_request)

Добавить внутренний комментарий

Добавить внутренний комментарий

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$order_add_comment_request = new \Ensi\OmsClient\Dto\OrderAddCommentRequest(); // \Ensi\OmsClient\Dto\OrderAddCommentRequest | 

try {
    $result = $apiInstance->addOrderComment($id, $order_add_comment_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->addOrderComment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **order_add_comment_request** | [**\Ensi\OmsClient\Dto\OrderAddCommentRequest**](../Model/OrderAddCommentRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## attachOrderFile

> \Ensi\OmsClient\Dto\OrderAttachFileResponse attachOrderFile($id, $file)

Прикрепить файл к заказу

Прикрепить файл к заказу

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->attachOrderFile($id, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->attachOrderFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\OmsClient\Dto\OrderAttachFileResponse**](../Model/OrderAttachFileResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## changeOrderDelivery

> \Ensi\OmsClient\Dto\OrderResponse changeOrderDelivery($id, $order_delivery_for_patch)

Обновление полей по доставке объекта типа Order

Обновление части полей по доставке объекта типа Order

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$order_delivery_for_patch = new \Ensi\OmsClient\Dto\OrderDeliveryForPatch(); // \Ensi\OmsClient\Dto\OrderDeliveryForPatch | 

try {
    $result = $apiInstance->changeOrderDelivery($id, $order_delivery_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->changeOrderDelivery: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **order_delivery_for_patch** | [**\Ensi\OmsClient\Dto\OrderDeliveryForPatch**](../Model/OrderDeliveryForPatch.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\OrderResponse**](../Model/OrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## changeOrderPaymentSystem

> \Ensi\OmsClient\Dto\OrderResponse changeOrderPaymentSystem($id, $order_change_payment_system_request)

Изменить систему оплаты заказа

Изменить систему оплаты заказа

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$order_change_payment_system_request = new \Ensi\OmsClient\Dto\OrderChangePaymentSystemRequest(); // \Ensi\OmsClient\Dto\OrderChangePaymentSystemRequest | 

try {
    $result = $apiInstance->changeOrderPaymentSystem($id, $order_change_payment_system_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->changeOrderPaymentSystem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **order_change_payment_system_request** | [**\Ensi\OmsClient\Dto\OrderChangePaymentSystemRequest**](../Model/OrderChangePaymentSystemRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\OrderResponse**](../Model/OrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## checkOrderPayment

> \Ensi\OmsClient\Dto\OrderCheckPaymentResponse checkOrderPayment($id)

Проверить статус оплаты заказа

Проверить статус оплаты заказа

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->checkOrderPayment($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->checkOrderPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\OmsClient\Dto\OrderCheckPaymentResponse**](../Model/OrderCheckPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## commitOrder

> \Ensi\OmsClient\Dto\OrderCommitResponse commitOrder($order_commit_request)

Оформить заказ

Оформить заказ

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$order_commit_request = new \Ensi\OmsClient\Dto\OrderCommitRequest(); // \Ensi\OmsClient\Dto\OrderCommitRequest | 

try {
    $result = $apiInstance->commitOrder($order_commit_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->commitOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **order_commit_request** | [**\Ensi\OmsClient\Dto\OrderCommitRequest**](../Model/OrderCommitRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\OrderCommitResponse**](../Model/OrderCommitResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteOrderFiles

> \Ensi\OmsClient\Dto\EmptyDataResponse deleteOrderFiles($id, $order_delete_files_request)

Удалить прикрепленные файлы к заказу

Удалить прикрепленные файлы к заказу

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$order_delete_files_request = new \Ensi\OmsClient\Dto\OrderDeleteFilesRequest(); // \Ensi\OmsClient\Dto\OrderDeleteFilesRequest | 

try {
    $result = $apiInstance->deleteOrderFiles($id, $order_delete_files_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->deleteOrderFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **order_delete_files_request** | [**\Ensi\OmsClient\Dto\OrderDeleteFilesRequest**](../Model/OrderDeleteFilesRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getOrder

> \Ensi\OmsClient\Dto\OrderResponse getOrder($id, $include)

Получение объекта типа Order

Получение объекта типа Order

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getOrder($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->getOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\OmsClient\Dto\OrderResponse**](../Model/OrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchOrder

> \Ensi\OmsClient\Dto\OrderResponse patchOrder($id, $order_for_patch)

Обновление части полей объекта типа Order

Обновление части полей объекта типа Order

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$order_for_patch = new \Ensi\OmsClient\Dto\OrderForPatch(); // \Ensi\OmsClient\Dto\OrderForPatch | 

try {
    $result = $apiInstance->patchOrder($id, $order_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->patchOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **order_for_patch** | [**\Ensi\OmsClient\Dto\OrderForPatch**](../Model/OrderForPatch.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\OrderResponse**](../Model/OrderResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOrders

> \Ensi\OmsClient\Dto\SearchOrdersResponse searchOrders($search_orders_request)

Поиск объектов типа Order

Поиск объектов типа Order

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_orders_request = new \Ensi\OmsClient\Dto\SearchOrdersRequest(); // \Ensi\OmsClient\Dto\SearchOrdersRequest | 

try {
    $result = $apiInstance->searchOrders($search_orders_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->searchOrders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_orders_request** | [**\Ensi\OmsClient\Dto\SearchOrdersRequest**](../Model/SearchOrdersRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\SearchOrdersResponse**](../Model/SearchOrdersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## startOrderPayment

> \Ensi\OmsClient\Dto\OrderStartPaymentResponse startOrderPayment($id, $order_start_payment_request)

Запустить оплату

Создать оплату во внешней системе (если это не было сделано ранее) и получить ссылку

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\OrdersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$order_start_payment_request = new \Ensi\OmsClient\Dto\OrderStartPaymentRequest(); // \Ensi\OmsClient\Dto\OrderStartPaymentRequest | 

try {
    $result = $apiInstance->startOrderPayment($id, $order_start_payment_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->startOrderPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **order_start_payment_request** | [**\Ensi\OmsClient\Dto\OrderStartPaymentRequest**](../Model/OrderStartPaymentRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\OrderStartPaymentResponse**](../Model/OrderStartPaymentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

