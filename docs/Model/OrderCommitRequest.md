# # OrderCommitRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_id** | **int** | Пользователь, который оформляет заказ | [optional] 
**customer_email** | **string** | Почта пользователя, который оформляет заказ | [optional] 
**client_comment** | **string** | Комментарий клиента к заказу | [optional] 
**payment_system_id** | **int** | система оплаты из PaymentSystemEnum | [optional] 
**source** | **int** | источник заказа из OrderSourceEnum | [optional] 
**spent_bonus** | **int** | Кол-во бонусов, которое списывается с пользователя в этом заказе | [optional] 
**added_bonus** | **int** | Кол-во бонусов, которое начислится пользователю за этот заказе | [optional] 
**delivery_service** | **int** | сервис доставки из логистики | [optional] 
**delivery_method** | **int** | метод доставки из логистики | [optional] 
**delivery_tariff_id** | **int** | Тариф, по которому пользователь решил доставлять | [optional] 
**delivery_point_id** | **int** | Пункт самовывода | [optional] 
**delivery_address** | [**\Ensi\OmsClient\Dto\Address**](Address.md) |  | [optional] 
**delivery_comment** | **string** | Комментарий клиента к доставке | [optional] 
**delivery_price** | **int** | Стоимость доставки со скидкой (в копейках) | [optional] 
**delivery_cost** | **int** | Стоимость доставки до скидок (в копейках) | [optional] 
**receiver_name** | **string** | Получатель. ФИО | [optional] 
**receiver_phone** | **string** | Получатель. Телефон | [optional] 
**receiver_email** | **string** | Получатель. Почта | [optional] 
**deliveries** | [**\Ensi\OmsClient\Dto\OrderCommitRequestDeliveries[]**](OrderCommitRequestDeliveries.md) | Информация об отправлениях | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


