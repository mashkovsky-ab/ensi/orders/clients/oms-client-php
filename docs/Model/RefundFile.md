# # RefundFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор | [optional] 
**refund_id** | **int** | идентификатор заявки на возврат | [optional] 
**original_name** | **string** | Оригинальное название файла | [optional] 
**file** | [**\Ensi\OmsClient\Dto\File**](File.md) |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата загрузки файла | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления файла | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


