# # RefundReasonForCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** | Символьный код причины возврата | [optional] 
**name** | **string** | Название причины возврата | [optional] 
**description** | **string** | Детальное описание причины возврата | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


