<?php
/**
 * Refund
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * OMS
 *
 * Управление заказами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\OmsClient\Dto;

use \ArrayAccess;
use \Ensi\OmsClient\ObjectSerializer;

/**
 * Refund Class Doc Comment
 *
 * @category Class
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class Refund implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'Refund';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'int',
        'price' => 'int',
        'is_partial' => 'bool',
        'files' => '\Ensi\OmsClient\Dto\RefundFile[]',
        'created_at' => '\DateTime',
        'updated_at' => '\DateTime',
        'status' => 'int',
        'responsible_id' => 'int',
        'rejection_comment' => 'string',
        'order_id' => 'int',
        'manager_id' => 'int',
        'source' => 'int',
        'user_comment' => 'string',
        'order' => '\Ensi\OmsClient\Dto\Order',
        'items' => '\Ensi\OmsClient\Dto\OrderItem[]',
        'reasons' => '\Ensi\OmsClient\Dto\RefundReason[]'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'id' => null,
        'price' => null,
        'is_partial' => null,
        'files' => null,
        'created_at' => 'date-time',
        'updated_at' => 'date-time',
        'status' => null,
        'responsible_id' => null,
        'rejection_comment' => null,
        'order_id' => null,
        'manager_id' => null,
        'source' => null,
        'user_comment' => null,
        'order' => null,
        'items' => null,
        'reasons' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static $openAPINullables = [
        'id' => false,
        'price' => false,
        'is_partial' => false,
        'files' => false,
        'created_at' => false,
        'updated_at' => false,
        'status' => false,
        'responsible_id' => true,
        'rejection_comment' => true,
        'order_id' => false,
        'manager_id' => true,
        'source' => false,
        'user_comment' => false,
        'order' => false,
        'items' => false,
        'reasons' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

        /**
     * Array of property to nullable mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPINullables()
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return array
     */
    public function getOpenAPINullablesSetToNull()
    {
        return $this->openAPINullablesSetToNull;
    }

    public function setOpenAPINullablesSetToNull($nullablesSetToNull)
    {
        $this->openAPINullablesSetToNull=$nullablesSetToNull;
        return $this;
    }

    /**
     * Checks if a property is nullable
     *
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        if (isset(self::$openAPINullables[$property])) {
            return self::$openAPINullables[$property];
        }

        return false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        if (in_array($property, $this->getOpenAPINullablesSetToNull())) {
            return true;
        }
        return false;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'price' => 'price',
        'is_partial' => 'is_partial',
        'files' => 'files',
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'status' => 'status',
        'responsible_id' => 'responsible_id',
        'rejection_comment' => 'rejection_comment',
        'order_id' => 'order_id',
        'manager_id' => 'manager_id',
        'source' => 'source',
        'user_comment' => 'user_comment',
        'order' => 'order',
        'items' => 'items',
        'reasons' => 'reasons'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'price' => 'setPrice',
        'is_partial' => 'setIsPartial',
        'files' => 'setFiles',
        'created_at' => 'setCreatedAt',
        'updated_at' => 'setUpdatedAt',
        'status' => 'setStatus',
        'responsible_id' => 'setResponsibleId',
        'rejection_comment' => 'setRejectionComment',
        'order_id' => 'setOrderId',
        'manager_id' => 'setManagerId',
        'source' => 'setSource',
        'user_comment' => 'setUserComment',
        'order' => 'setOrder',
        'items' => 'setItems',
        'reasons' => 'setReasons'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'price' => 'getPrice',
        'is_partial' => 'getIsPartial',
        'files' => 'getFiles',
        'created_at' => 'getCreatedAt',
        'updated_at' => 'getUpdatedAt',
        'status' => 'getStatus',
        'responsible_id' => 'getResponsibleId',
        'rejection_comment' => 'getRejectionComment',
        'order_id' => 'getOrderId',
        'manager_id' => 'getManagerId',
        'source' => 'getSource',
        'user_comment' => 'getUserComment',
        'order' => 'getOrder',
        'items' => 'getItems',
        'reasons' => 'getReasons'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('id', $data, null);
        $this->setIfExists('price', $data, null);
        $this->setIfExists('is_partial', $data, null);
        $this->setIfExists('files', $data, null);
        $this->setIfExists('created_at', $data, null);
        $this->setIfExists('updated_at', $data, null);
        $this->setIfExists('status', $data, null);
        $this->setIfExists('responsible_id', $data, null);
        $this->setIfExists('rejection_comment', $data, null);
        $this->setIfExists('order_id', $data, null);
        $this->setIfExists('manager_id', $data, null);
        $this->setIfExists('source', $data, null);
        $this->setIfExists('user_comment', $data, null);
        $this->setIfExists('order', $data, null);
        $this->setIfExists('items', $data, null);
        $this->setIfExists('reasons', $data, null);
    }

    public function setIfExists(string $variableName, $fields, $defaultValue)
    {
        if (is_array($fields) && array_key_exists($variableName, $fields) && is_null($fields[$variableName]) && self::isNullable($variableName)) {
            array_push($this->openAPINullablesSetToNull, $variableName);
        }

        $this->container[$variableName] = isset($fields[$variableName]) ? $fields[$variableName] : $defaultValue;

        return $this;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int|null $id идентификатор
     *
     * @return $this
     */
    public function setId($id)
    {


        /*if (is_null($id)) {
            throw new \InvalidArgumentException('non-nullable id cannot be null');
        }*/
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets price
     *
     * @return int|null
     */
    public function getPrice()
    {
        return $this->container['price'];
    }

    /**
     * Sets price
     *
     * @param int|null $price сумма возврата, коп.
     *
     * @return $this
     */
    public function setPrice($price)
    {


        /*if (is_null($price)) {
            throw new \InvalidArgumentException('non-nullable price cannot be null');
        }*/
        $this->container['price'] = $price;

        return $this;
    }

    /**
     * Gets is_partial
     *
     * @return bool|null
     */
    public function getIsPartial()
    {
        return $this->container['is_partial'];
    }

    /**
     * Sets is_partial
     *
     * @param bool|null $is_partial является заявкой на частичный возврат
     *
     * @return $this
     */
    public function setIsPartial($is_partial)
    {


        /*if (is_null($is_partial)) {
            throw new \InvalidArgumentException('non-nullable is_partial cannot be null');
        }*/
        $this->container['is_partial'] = $is_partial;

        return $this;
    }

    /**
     * Gets files
     *
     * @return \Ensi\OmsClient\Dto\RefundFile[]|null
     */
    public function getFiles()
    {
        return $this->container['files'];
    }

    /**
     * Sets files
     *
     * @param \Ensi\OmsClient\Dto\RefundFile[]|null $files files
     *
     * @return $this
     */
    public function setFiles($files)
    {


        /*if (is_null($files)) {
            throw new \InvalidArgumentException('non-nullable files cannot be null');
        }*/
        $this->container['files'] = $files;

        return $this;
    }

    /**
     * Gets created_at
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->container['created_at'];
    }

    /**
     * Sets created_at
     *
     * @param \DateTime|null $created_at дата создания
     *
     * @return $this
     */
    public function setCreatedAt($created_at)
    {


        /*if (is_null($created_at)) {
            throw new \InvalidArgumentException('non-nullable created_at cannot be null');
        }*/
        $this->container['created_at'] = $created_at;

        return $this;
    }

    /**
     * Gets updated_at
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->container['updated_at'];
    }

    /**
     * Sets updated_at
     *
     * @param \DateTime|null $updated_at дата обновления
     *
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {


        /*if (is_null($updated_at)) {
            throw new \InvalidArgumentException('non-nullable updated_at cannot be null');
        }*/
        $this->container['updated_at'] = $updated_at;

        return $this;
    }

    /**
     * Gets status
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->container['status'];
    }

    /**
     * Sets status
     *
     * @param int|null $status статус заявки на возврат из RefundStatusEnum
     *
     * @return $this
     */
    public function setStatus($status)
    {


        /*if (is_null($status)) {
            throw new \InvalidArgumentException('non-nullable status cannot be null');
        }*/
        $this->container['status'] = $status;

        return $this;
    }

    /**
     * Gets responsible_id
     *
     * @return int|null
     */
    public function getResponsibleId()
    {
        return $this->container['responsible_id'];
    }

    /**
     * Sets responsible_id
     *
     * @param int|null $responsible_id идентификатор ответственного
     *
     * @return $this
     */
    public function setResponsibleId($responsible_id)
    {

        if (is_null($responsible_id)) {
            array_push($this->openAPINullablesSetToNull, 'responsible_id');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('responsible_id', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }

        $this->container['responsible_id'] = $responsible_id;

        return $this;
    }

    /**
     * Gets rejection_comment
     *
     * @return string|null
     */
    public function getRejectionComment()
    {
        return $this->container['rejection_comment'];
    }

    /**
     * Sets rejection_comment
     *
     * @param string|null $rejection_comment причина отклонения
     *
     * @return $this
     */
    public function setRejectionComment($rejection_comment)
    {

        if (is_null($rejection_comment)) {
            array_push($this->openAPINullablesSetToNull, 'rejection_comment');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('rejection_comment', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }

        $this->container['rejection_comment'] = $rejection_comment;

        return $this;
    }

    /**
     * Gets order_id
     *
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->container['order_id'];
    }

    /**
     * Sets order_id
     *
     * @param int|null $order_id идентификатор заказа
     *
     * @return $this
     */
    public function setOrderId($order_id)
    {


        /*if (is_null($order_id)) {
            throw new \InvalidArgumentException('non-nullable order_id cannot be null');
        }*/
        $this->container['order_id'] = $order_id;

        return $this;
    }

    /**
     * Gets manager_id
     *
     * @return int|null
     */
    public function getManagerId()
    {
        return $this->container['manager_id'];
    }

    /**
     * Sets manager_id
     *
     * @param int|null $manager_id идентификатор администратора (если автором был администратор)
     *
     * @return $this
     */
    public function setManagerId($manager_id)
    {

        if (is_null($manager_id)) {
            array_push($this->openAPINullablesSetToNull, 'manager_id');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('manager_id', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }

        $this->container['manager_id'] = $manager_id;

        return $this;
    }

    /**
     * Gets source
     *
     * @return int|null
     */
    public function getSource()
    {
        return $this->container['source'];
    }

    /**
     * Sets source
     *
     * @param int|null $source источник взаимодействия (канал)
     *
     * @return $this
     */
    public function setSource($source)
    {


        /*if (is_null($source)) {
            throw new \InvalidArgumentException('non-nullable source cannot be null');
        }*/
        $this->container['source'] = $source;

        return $this;
    }

    /**
     * Gets user_comment
     *
     * @return string|null
     */
    public function getUserComment()
    {
        return $this->container['user_comment'];
    }

    /**
     * Sets user_comment
     *
     * @param string|null $user_comment комментарий пользователя
     *
     * @return $this
     */
    public function setUserComment($user_comment)
    {


        /*if (is_null($user_comment)) {
            throw new \InvalidArgumentException('non-nullable user_comment cannot be null');
        }*/
        $this->container['user_comment'] = $user_comment;

        return $this;
    }

    /**
     * Gets order
     *
     * @return \Ensi\OmsClient\Dto\Order|null
     */
    public function getOrder()
    {
        return $this->container['order'];
    }

    /**
     * Sets order
     *
     * @param \Ensi\OmsClient\Dto\Order|null $order order
     *
     * @return $this
     */
    public function setOrder($order)
    {


        /*if (is_null($order)) {
            throw new \InvalidArgumentException('non-nullable order cannot be null');
        }*/
        $this->container['order'] = $order;

        return $this;
    }

    /**
     * Gets items
     *
     * @return \Ensi\OmsClient\Dto\OrderItem[]|null
     */
    public function getItems()
    {
        return $this->container['items'];
    }

    /**
     * Sets items
     *
     * @param \Ensi\OmsClient\Dto\OrderItem[]|null $items items
     *
     * @return $this
     */
    public function setItems($items)
    {


        /*if (is_null($items)) {
            throw new \InvalidArgumentException('non-nullable items cannot be null');
        }*/
        $this->container['items'] = $items;

        return $this;
    }

    /**
     * Gets reasons
     *
     * @return \Ensi\OmsClient\Dto\RefundReason[]|null
     */
    public function getReasons()
    {
        return $this->container['reasons'];
    }

    /**
     * Sets reasons
     *
     * @param \Ensi\OmsClient\Dto\RefundReason[]|null $reasons reasons
     *
     * @return $this
     */
    public function setReasons($reasons)
    {


        /*if (is_null($reasons)) {
            throw new \InvalidArgumentException('non-nullable reasons cannot be null');
        }*/
        $this->container['reasons'] = $reasons;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


