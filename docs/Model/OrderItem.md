# # OrderItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id элемента корзины | [optional] 
**order_id** | **int** | ID заказа | [optional] 
**shipment_id** | **int** | ID отгрузки | [optional] 
**offer_id** | **int** | ID оффера | [optional] 
**name** | **string** | Название товара | [optional] 
**qty** | **float** | Кол-во товара | [optional] 
**price** | **int** | Цена товара (цена * qty - скидки) (в коп.) | [optional] 
**price_per_one** | **int** | Цена единичного товара (в коп.) | [optional] 
**cost** | **int** | Цена товара до скидок (цена * qty) (в коп.) | [optional] 
**cost_per_one** | **int** | Цена единичного товара до скидок (в коп.) | [optional] 
**product_weight** | **float** | Вес нетто товара (кг) | [optional] 
**product_weight_gross** | **float** | Вес брутто товара (кг) | [optional] 
**product_width** | **float** | Ширина товара (мм) | [optional] 
**product_height** | **float** | Высота товара (мм) | [optional] 
**product_length** | **float** | Длина товара (мм) | [optional] 
**offer_external_id** | **string** | внешний ID товара | [optional] 
**offer_storage_address** | **string** | место хранения в магазине | [optional] 
**product_storage_area** | **string** | условия хранения | [optional] 
**product_barcode** | **string** | артикул (EAN) | [optional] 
**refund_qty** | **float** | Количество возвращаемых элементов корзины в заявке | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


