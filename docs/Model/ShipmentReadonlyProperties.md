# # ShipmentReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор | [optional] 
**number** | **string** | номер отгрузки | [optional] 
**delivery_id** | **int** | ид отправления | [optional] 
**seller_id** | **int** | ид продавца | [optional] 
**store_id** | **int** | ид склада | [optional] 
**status_at** | [**\DateTime**](\DateTime.md) | дата установки статуса | [optional] 
**cost** | **int** | сумма (в копейках) товаров отгрузки (рассчитывается автоматически) | [optional] 
**width** | **float** | ширина (рассчитывается автоматически) | [optional] 
**height** | **float** | высота (рассчитывается автоматически) | [optional] 
**length** | **float** | длина (рассчитывается автоматически) | [optional] 
**weight** | **float** | вес (рассчитывается автоматически) | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


