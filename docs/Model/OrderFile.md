# # OrderFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID файла | [optional] 
**order_id** | **int** | ID заказа | [optional] 
**original_name** | **string** | Оригинальное название файла | [optional] 
**file** | [**\Ensi\OmsClient\Dto\File**](File.md) |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания файла | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления файла | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


