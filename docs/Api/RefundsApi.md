# Ensi\OmsClient\RefundsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**attachRefundFile**](RefundsApi.md#attachRefundFile) | **POST** /refunds/refunds/{id}:attach-file | Загрузка вложения для заявки на возврат
[**createRefund**](RefundsApi.md#createRefund) | **POST** /refunds/refunds | Создание объекта типа Refund
[**deleteRefundFiles**](RefundsApi.md#deleteRefundFiles) | **DELETE** /refunds/refunds/{id}:delete-files | Удаление вложений для заявки на возврат
[**getRefund**](RefundsApi.md#getRefund) | **GET** /refunds/refunds/{id} | Получение объекта типа Refund
[**patchRefund**](RefundsApi.md#patchRefund) | **PATCH** /refunds/refunds/{id} | Обновление части полей объекта типа Refund
[**searchRefunds**](RefundsApi.md#searchRefunds) | **POST** /refunds/refunds:search | Поиск объектов типа Refund



## attachRefundFile

> \Ensi\OmsClient\Dto\RefundFileResponse attachRefundFile($id, $file)

Загрузка вложения для заявки на возврат

Загрузка вложения для заявки на возврат

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->attachRefundFile($id, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->attachRefundFile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\OmsClient\Dto\RefundFileResponse**](../Model/RefundFileResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createRefund

> \Ensi\OmsClient\Dto\RefundResponse createRefund($refund_for_create)

Создание объекта типа Refund

Создание объекта типа Refund

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$refund_for_create = new \Ensi\OmsClient\Dto\RefundForCreate(); // \Ensi\OmsClient\Dto\RefundForCreate | 

try {
    $result = $apiInstance->createRefund($refund_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->createRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **refund_for_create** | [**\Ensi\OmsClient\Dto\RefundForCreate**](../Model/RefundForCreate.md)|  | [optional]

### Return type

[**\Ensi\OmsClient\Dto\RefundResponse**](../Model/RefundResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteRefundFiles

> \Ensi\OmsClient\Dto\EmptyDataResponse deleteRefundFiles($id, $refund_for_delete_files)

Удаление вложений для заявки на возврат

Удаление вложений для заявки на возврат

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$refund_for_delete_files = new \Ensi\OmsClient\Dto\RefundForDeleteFiles(); // \Ensi\OmsClient\Dto\RefundForDeleteFiles | 

try {
    $result = $apiInstance->deleteRefundFiles($id, $refund_for_delete_files);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->deleteRefundFiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **refund_for_delete_files** | [**\Ensi\OmsClient\Dto\RefundForDeleteFiles**](../Model/RefundForDeleteFiles.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getRefund

> \Ensi\OmsClient\Dto\RefundResponse getRefund($id, $include)

Получение объекта типа Refund

Получение объекта типа Refund

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getRefund($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->getRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\OmsClient\Dto\RefundResponse**](../Model/RefundResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchRefund

> \Ensi\OmsClient\Dto\RefundResponse patchRefund($id, $refund_for_patch)

Обновление части полей объекта типа Refund

Обновление части полей объекта типа Refund

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$refund_for_patch = new \Ensi\OmsClient\Dto\RefundForPatch(); // \Ensi\OmsClient\Dto\RefundForPatch | 

try {
    $result = $apiInstance->patchRefund($id, $refund_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->patchRefund: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **refund_for_patch** | [**\Ensi\OmsClient\Dto\RefundForPatch**](../Model/RefundForPatch.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\RefundResponse**](../Model/RefundResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchRefunds

> \Ensi\OmsClient\Dto\SearchRefundsResponse searchRefunds($search_refunds_request)

Поиск объектов типа Refund

Поиск объектов типа Refund

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\RefundsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_refunds_request = new \Ensi\OmsClient\Dto\SearchRefundsRequest(); // \Ensi\OmsClient\Dto\SearchRefundsRequest | 

try {
    $result = $apiInstance->searchRefunds($search_refunds_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RefundsApi->searchRefunds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_refunds_request** | [**\Ensi\OmsClient\Dto\SearchRefundsRequest**](../Model/SearchRefundsRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\SearchRefundsResponse**](../Model/SearchRefundsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

