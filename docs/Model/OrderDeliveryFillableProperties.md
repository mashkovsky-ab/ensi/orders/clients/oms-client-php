# # OrderDeliveryFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delivery_service** | **int** | служба доставки | [optional] 
**delivery_method** | **int** | метод доставки | [optional] 
**delivery_cost** | **int** | стоимость доставки (без учета скидки) в копейках | [optional] 
**delivery_price** | **int** | стоимость доставки (с учетом скидки) в копейках | [optional] 
**delivery_tariff_id** | **int** | ID тарифа на доставку из сервиса логистики | [optional] 
**delivery_point_id** | **int** | ID пункта самовывоза из сервиса логистики | [optional] 
**delivery_address** | [**\Ensi\OmsClient\Dto\Address**](Address.md) |  | [optional] 
**delivery_comment** | **string** | комментарий к доставке | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


