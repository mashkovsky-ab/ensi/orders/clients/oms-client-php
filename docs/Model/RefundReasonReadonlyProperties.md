# # RefundReasonReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


