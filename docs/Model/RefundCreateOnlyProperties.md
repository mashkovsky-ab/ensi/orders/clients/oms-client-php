# # RefundCreateOnlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **int** | идентификатор заказа | [optional] 
**manager_id** | **int** | идентификатор администратора (если автором был администратор) | [optional] 
**source** | **int** | источник взаимодействия (канал) | [optional] 
**user_comment** | **string** | комментарий пользователя | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


