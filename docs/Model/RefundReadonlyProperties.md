# # RefundReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор | [optional] 
**price** | **int** | сумма возврата, коп. | [optional] 
**is_partial** | **bool** | является заявкой на частичный возврат | [optional] 
**files** | [**\Ensi\OmsClient\Dto\RefundFile[]**](RefundFile.md) |  | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


