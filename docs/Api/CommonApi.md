# Ensi\OmsClient\CommonApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**patchSettings**](CommonApi.md#patchSettings) | **PATCH** /common/settings | Обновление параметров
[**searchSettings**](CommonApi.md#searchSettings) | **GET** /common/settings | Получение всех параметров



## patchSettings

> \Ensi\OmsClient\Dto\SettingsResponse patchSettings($patch_several_settings_request)

Обновление параметров

Обновление параметров

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$patch_several_settings_request = new \Ensi\OmsClient\Dto\PatchSeveralSettingsRequest(); // \Ensi\OmsClient\Dto\PatchSeveralSettingsRequest | 

try {
    $result = $apiInstance->patchSettings($patch_several_settings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->patchSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_several_settings_request** | [**\Ensi\OmsClient\Dto\PatchSeveralSettingsRequest**](../Model/PatchSeveralSettingsRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\SettingsResponse**](../Model/SettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchSettings

> \Ensi\OmsClient\Dto\SettingsResponse searchSettings()

Получение всех параметров

Получение всех параметров

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\CommonApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->searchSettings();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommonApi->searchSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\SettingsResponse**](../Model/SettingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

