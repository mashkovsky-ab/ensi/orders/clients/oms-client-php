<?php
/**
 * OrderItem
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * OMS
 *
 * Управление заказами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\OmsClient\Dto;

use \ArrayAccess;
use \Ensi\OmsClient\ObjectSerializer;

/**
 * OrderItem Class Doc Comment
 *
 * @category Class
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class OrderItem implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'OrderItem';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'int',
        'order_id' => 'int',
        'shipment_id' => 'int',
        'offer_id' => 'int',
        'name' => 'string',
        'qty' => 'float',
        'price' => 'int',
        'price_per_one' => 'int',
        'cost' => 'int',
        'cost_per_one' => 'int',
        'product_weight' => 'float',
        'product_weight_gross' => 'float',
        'product_width' => 'float',
        'product_height' => 'float',
        'product_length' => 'float',
        'offer_external_id' => 'string',
        'offer_storage_address' => 'string',
        'product_storage_area' => 'string',
        'product_barcode' => 'string',
        'refund_qty' => 'float',
        'created_at' => '\DateTime',
        'updated_at' => '\DateTime'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'id' => null,
        'order_id' => null,
        'shipment_id' => null,
        'offer_id' => null,
        'name' => null,
        'qty' => null,
        'price' => null,
        'price_per_one' => null,
        'cost' => null,
        'cost_per_one' => null,
        'product_weight' => null,
        'product_weight_gross' => null,
        'product_width' => null,
        'product_height' => null,
        'product_length' => null,
        'offer_external_id' => null,
        'offer_storage_address' => null,
        'product_storage_area' => null,
        'product_barcode' => null,
        'refund_qty' => null,
        'created_at' => 'date-time',
        'updated_at' => 'date-time'
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static $openAPINullables = [
        'id' => false,
        'order_id' => false,
        'shipment_id' => false,
        'offer_id' => false,
        'name' => false,
        'qty' => false,
        'price' => false,
        'price_per_one' => false,
        'cost' => false,
        'cost_per_one' => false,
        'product_weight' => false,
        'product_weight_gross' => false,
        'product_width' => false,
        'product_height' => false,
        'product_length' => false,
        'offer_external_id' => false,
        'offer_storage_address' => true,
        'product_storage_area' => true,
        'product_barcode' => true,
        'refund_qty' => true,
        'created_at' => false,
        'updated_at' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

        /**
     * Array of property to nullable mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPINullables()
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return array
     */
    public function getOpenAPINullablesSetToNull()
    {
        return $this->openAPINullablesSetToNull;
    }

    public function setOpenAPINullablesSetToNull($nullablesSetToNull)
    {
        $this->openAPINullablesSetToNull=$nullablesSetToNull;
        return $this;
    }

    /**
     * Checks if a property is nullable
     *
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        if (isset(self::$openAPINullables[$property])) {
            return self::$openAPINullables[$property];
        }

        return false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        if (in_array($property, $this->getOpenAPINullablesSetToNull())) {
            return true;
        }
        return false;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'order_id' => 'order_id',
        'shipment_id' => 'shipment_id',
        'offer_id' => 'offer_id',
        'name' => 'name',
        'qty' => 'qty',
        'price' => 'price',
        'price_per_one' => 'price_per_one',
        'cost' => 'cost',
        'cost_per_one' => 'cost_per_one',
        'product_weight' => 'product_weight',
        'product_weight_gross' => 'product_weight_gross',
        'product_width' => 'product_width',
        'product_height' => 'product_height',
        'product_length' => 'product_length',
        'offer_external_id' => 'offer_external_id',
        'offer_storage_address' => 'offer_storage_address',
        'product_storage_area' => 'product_storage_area',
        'product_barcode' => 'product_barcode',
        'refund_qty' => 'refund_qty',
        'created_at' => 'created_at',
        'updated_at' => 'updated_at'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'order_id' => 'setOrderId',
        'shipment_id' => 'setShipmentId',
        'offer_id' => 'setOfferId',
        'name' => 'setName',
        'qty' => 'setQty',
        'price' => 'setPrice',
        'price_per_one' => 'setPricePerOne',
        'cost' => 'setCost',
        'cost_per_one' => 'setCostPerOne',
        'product_weight' => 'setProductWeight',
        'product_weight_gross' => 'setProductWeightGross',
        'product_width' => 'setProductWidth',
        'product_height' => 'setProductHeight',
        'product_length' => 'setProductLength',
        'offer_external_id' => 'setOfferExternalId',
        'offer_storage_address' => 'setOfferStorageAddress',
        'product_storage_area' => 'setProductStorageArea',
        'product_barcode' => 'setProductBarcode',
        'refund_qty' => 'setRefundQty',
        'created_at' => 'setCreatedAt',
        'updated_at' => 'setUpdatedAt'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'order_id' => 'getOrderId',
        'shipment_id' => 'getShipmentId',
        'offer_id' => 'getOfferId',
        'name' => 'getName',
        'qty' => 'getQty',
        'price' => 'getPrice',
        'price_per_one' => 'getPricePerOne',
        'cost' => 'getCost',
        'cost_per_one' => 'getCostPerOne',
        'product_weight' => 'getProductWeight',
        'product_weight_gross' => 'getProductWeightGross',
        'product_width' => 'getProductWidth',
        'product_height' => 'getProductHeight',
        'product_length' => 'getProductLength',
        'offer_external_id' => 'getOfferExternalId',
        'offer_storage_address' => 'getOfferStorageAddress',
        'product_storage_area' => 'getProductStorageArea',
        'product_barcode' => 'getProductBarcode',
        'refund_qty' => 'getRefundQty',
        'created_at' => 'getCreatedAt',
        'updated_at' => 'getUpdatedAt'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('id', $data, null);
        $this->setIfExists('order_id', $data, null);
        $this->setIfExists('shipment_id', $data, null);
        $this->setIfExists('offer_id', $data, null);
        $this->setIfExists('name', $data, null);
        $this->setIfExists('qty', $data, null);
        $this->setIfExists('price', $data, null);
        $this->setIfExists('price_per_one', $data, null);
        $this->setIfExists('cost', $data, null);
        $this->setIfExists('cost_per_one', $data, null);
        $this->setIfExists('product_weight', $data, null);
        $this->setIfExists('product_weight_gross', $data, null);
        $this->setIfExists('product_width', $data, null);
        $this->setIfExists('product_height', $data, null);
        $this->setIfExists('product_length', $data, null);
        $this->setIfExists('offer_external_id', $data, null);
        $this->setIfExists('offer_storage_address', $data, null);
        $this->setIfExists('product_storage_area', $data, null);
        $this->setIfExists('product_barcode', $data, null);
        $this->setIfExists('refund_qty', $data, null);
        $this->setIfExists('created_at', $data, null);
        $this->setIfExists('updated_at', $data, null);
    }

    public function setIfExists(string $variableName, $fields, $defaultValue)
    {
        if (is_array($fields) && array_key_exists($variableName, $fields) && is_null($fields[$variableName]) && self::isNullable($variableName)) {
            array_push($this->openAPINullablesSetToNull, $variableName);
        }

        $this->container[$variableName] = isset($fields[$variableName]) ? $fields[$variableName] : $defaultValue;

        return $this;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int|null $id id элемента корзины
     *
     * @return $this
     */
    public function setId($id)
    {


        /*if (is_null($id)) {
            throw new \InvalidArgumentException('non-nullable id cannot be null');
        }*/
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets order_id
     *
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->container['order_id'];
    }

    /**
     * Sets order_id
     *
     * @param int|null $order_id ID заказа
     *
     * @return $this
     */
    public function setOrderId($order_id)
    {


        /*if (is_null($order_id)) {
            throw new \InvalidArgumentException('non-nullable order_id cannot be null');
        }*/
        $this->container['order_id'] = $order_id;

        return $this;
    }

    /**
     * Gets shipment_id
     *
     * @return int|null
     */
    public function getShipmentId()
    {
        return $this->container['shipment_id'];
    }

    /**
     * Sets shipment_id
     *
     * @param int|null $shipment_id ID отгрузки
     *
     * @return $this
     */
    public function setShipmentId($shipment_id)
    {


        /*if (is_null($shipment_id)) {
            throw new \InvalidArgumentException('non-nullable shipment_id cannot be null');
        }*/
        $this->container['shipment_id'] = $shipment_id;

        return $this;
    }

    /**
     * Gets offer_id
     *
     * @return int|null
     */
    public function getOfferId()
    {
        return $this->container['offer_id'];
    }

    /**
     * Sets offer_id
     *
     * @param int|null $offer_id ID оффера
     *
     * @return $this
     */
    public function setOfferId($offer_id)
    {


        /*if (is_null($offer_id)) {
            throw new \InvalidArgumentException('non-nullable offer_id cannot be null');
        }*/
        $this->container['offer_id'] = $offer_id;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string|null $name Название товара
     *
     * @return $this
     */
    public function setName($name)
    {


        /*if (is_null($name)) {
            throw new \InvalidArgumentException('non-nullable name cannot be null');
        }*/
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets qty
     *
     * @return float|null
     */
    public function getQty()
    {
        return $this->container['qty'];
    }

    /**
     * Sets qty
     *
     * @param float|null $qty Кол-во товара
     *
     * @return $this
     */
    public function setQty($qty)
    {


        /*if (is_null($qty)) {
            throw new \InvalidArgumentException('non-nullable qty cannot be null');
        }*/
        $this->container['qty'] = $qty;

        return $this;
    }

    /**
     * Gets price
     *
     * @return int|null
     */
    public function getPrice()
    {
        return $this->container['price'];
    }

    /**
     * Sets price
     *
     * @param int|null $price Цена товара (цена * qty - скидки) (в коп.)
     *
     * @return $this
     */
    public function setPrice($price)
    {


        /*if (is_null($price)) {
            throw new \InvalidArgumentException('non-nullable price cannot be null');
        }*/
        $this->container['price'] = $price;

        return $this;
    }

    /**
     * Gets price_per_one
     *
     * @return int|null
     */
    public function getPricePerOne()
    {
        return $this->container['price_per_one'];
    }

    /**
     * Sets price_per_one
     *
     * @param int|null $price_per_one Цена единичного товара (в коп.)
     *
     * @return $this
     */
    public function setPricePerOne($price_per_one)
    {


        /*if (is_null($price_per_one)) {
            throw new \InvalidArgumentException('non-nullable price_per_one cannot be null');
        }*/
        $this->container['price_per_one'] = $price_per_one;

        return $this;
    }

    /**
     * Gets cost
     *
     * @return int|null
     */
    public function getCost()
    {
        return $this->container['cost'];
    }

    /**
     * Sets cost
     *
     * @param int|null $cost Цена товара до скидок (цена * qty) (в коп.)
     *
     * @return $this
     */
    public function setCost($cost)
    {


        /*if (is_null($cost)) {
            throw new \InvalidArgumentException('non-nullable cost cannot be null');
        }*/
        $this->container['cost'] = $cost;

        return $this;
    }

    /**
     * Gets cost_per_one
     *
     * @return int|null
     */
    public function getCostPerOne()
    {
        return $this->container['cost_per_one'];
    }

    /**
     * Sets cost_per_one
     *
     * @param int|null $cost_per_one Цена единичного товара до скидок (в коп.)
     *
     * @return $this
     */
    public function setCostPerOne($cost_per_one)
    {


        /*if (is_null($cost_per_one)) {
            throw new \InvalidArgumentException('non-nullable cost_per_one cannot be null');
        }*/
        $this->container['cost_per_one'] = $cost_per_one;

        return $this;
    }

    /**
     * Gets product_weight
     *
     * @return float|null
     */
    public function getProductWeight()
    {
        return $this->container['product_weight'];
    }

    /**
     * Sets product_weight
     *
     * @param float|null $product_weight Вес нетто товара (кг)
     *
     * @return $this
     */
    public function setProductWeight($product_weight)
    {


        /*if (is_null($product_weight)) {
            throw new \InvalidArgumentException('non-nullable product_weight cannot be null');
        }*/
        $this->container['product_weight'] = $product_weight;

        return $this;
    }

    /**
     * Gets product_weight_gross
     *
     * @return float|null
     */
    public function getProductWeightGross()
    {
        return $this->container['product_weight_gross'];
    }

    /**
     * Sets product_weight_gross
     *
     * @param float|null $product_weight_gross Вес брутто товара (кг)
     *
     * @return $this
     */
    public function setProductWeightGross($product_weight_gross)
    {


        /*if (is_null($product_weight_gross)) {
            throw new \InvalidArgumentException('non-nullable product_weight_gross cannot be null');
        }*/
        $this->container['product_weight_gross'] = $product_weight_gross;

        return $this;
    }

    /**
     * Gets product_width
     *
     * @return float|null
     */
    public function getProductWidth()
    {
        return $this->container['product_width'];
    }

    /**
     * Sets product_width
     *
     * @param float|null $product_width Ширина товара (мм)
     *
     * @return $this
     */
    public function setProductWidth($product_width)
    {


        /*if (is_null($product_width)) {
            throw new \InvalidArgumentException('non-nullable product_width cannot be null');
        }*/
        $this->container['product_width'] = $product_width;

        return $this;
    }

    /**
     * Gets product_height
     *
     * @return float|null
     */
    public function getProductHeight()
    {
        return $this->container['product_height'];
    }

    /**
     * Sets product_height
     *
     * @param float|null $product_height Высота товара (мм)
     *
     * @return $this
     */
    public function setProductHeight($product_height)
    {


        /*if (is_null($product_height)) {
            throw new \InvalidArgumentException('non-nullable product_height cannot be null');
        }*/
        $this->container['product_height'] = $product_height;

        return $this;
    }

    /**
     * Gets product_length
     *
     * @return float|null
     */
    public function getProductLength()
    {
        return $this->container['product_length'];
    }

    /**
     * Sets product_length
     *
     * @param float|null $product_length Длина товара (мм)
     *
     * @return $this
     */
    public function setProductLength($product_length)
    {


        /*if (is_null($product_length)) {
            throw new \InvalidArgumentException('non-nullable product_length cannot be null');
        }*/
        $this->container['product_length'] = $product_length;

        return $this;
    }

    /**
     * Gets offer_external_id
     *
     * @return string|null
     */
    public function getOfferExternalId()
    {
        return $this->container['offer_external_id'];
    }

    /**
     * Sets offer_external_id
     *
     * @param string|null $offer_external_id внешний ID товара
     *
     * @return $this
     */
    public function setOfferExternalId($offer_external_id)
    {


        /*if (is_null($offer_external_id)) {
            throw new \InvalidArgumentException('non-nullable offer_external_id cannot be null');
        }*/
        $this->container['offer_external_id'] = $offer_external_id;

        return $this;
    }

    /**
     * Gets offer_storage_address
     *
     * @return string|null
     */
    public function getOfferStorageAddress()
    {
        return $this->container['offer_storage_address'];
    }

    /**
     * Sets offer_storage_address
     *
     * @param string|null $offer_storage_address место хранения в магазине
     *
     * @return $this
     */
    public function setOfferStorageAddress($offer_storage_address)
    {

        if (is_null($offer_storage_address)) {
            array_push($this->openAPINullablesSetToNull, 'offer_storage_address');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('offer_storage_address', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }

        $this->container['offer_storage_address'] = $offer_storage_address;

        return $this;
    }

    /**
     * Gets product_storage_area
     *
     * @return string|null
     */
    public function getProductStorageArea()
    {
        return $this->container['product_storage_area'];
    }

    /**
     * Sets product_storage_area
     *
     * @param string|null $product_storage_area условия хранения
     *
     * @return $this
     */
    public function setProductStorageArea($product_storage_area)
    {

        if (is_null($product_storage_area)) {
            array_push($this->openAPINullablesSetToNull, 'product_storage_area');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('product_storage_area', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }

        $this->container['product_storage_area'] = $product_storage_area;

        return $this;
    }

    /**
     * Gets product_barcode
     *
     * @return string|null
     */
    public function getProductBarcode()
    {
        return $this->container['product_barcode'];
    }

    /**
     * Sets product_barcode
     *
     * @param string|null $product_barcode артикул (EAN)
     *
     * @return $this
     */
    public function setProductBarcode($product_barcode)
    {

        if (is_null($product_barcode)) {
            array_push($this->openAPINullablesSetToNull, 'product_barcode');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('product_barcode', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }

        $this->container['product_barcode'] = $product_barcode;

        return $this;
    }

    /**
     * Gets refund_qty
     *
     * @return float|null
     */
    public function getRefundQty()
    {
        return $this->container['refund_qty'];
    }

    /**
     * Sets refund_qty
     *
     * @param float|null $refund_qty Количество возвращаемых элементов корзины в заявке
     *
     * @return $this
     */
    public function setRefundQty($refund_qty)
    {

        if (is_null($refund_qty)) {
            array_push($this->openAPINullablesSetToNull, 'refund_qty');
        } else {
            $nullablesSetToNull = $this->getOpenAPINullablesSetToNull();
            $index = array_search('refund_qty', $nullablesSetToNull);
            if ($index !== FALSE) {
                unset($nullablesSetToNull[$index]);
                $this->setOpenAPINullablesSetToNull($nullablesSetToNull);
            }
        }

        $this->container['refund_qty'] = $refund_qty;

        return $this;
    }

    /**
     * Gets created_at
     *
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->container['created_at'];
    }

    /**
     * Sets created_at
     *
     * @param \DateTime|null $created_at дата создания
     *
     * @return $this
     */
    public function setCreatedAt($created_at)
    {


        /*if (is_null($created_at)) {
            throw new \InvalidArgumentException('non-nullable created_at cannot be null');
        }*/
        $this->container['created_at'] = $created_at;

        return $this;
    }

    /**
     * Gets updated_at
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->container['updated_at'];
    }

    /**
     * Sets updated_at
     *
     * @param \DateTime|null $updated_at дата обновления
     *
     * @return $this
     */
    public function setUpdatedAt($updated_at)
    {


        /*if (is_null($updated_at)) {
            throw new \InvalidArgumentException('non-nullable updated_at cannot be null');
        }*/
        $this->container['updated_at'] = $updated_at;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


