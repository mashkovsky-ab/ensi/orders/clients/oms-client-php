# # OrderCommitRequestShipments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seller_id** | **int** | Продавец, чьи товары в этой отгрузке | [optional] 
**store_id** | **int** | Склад, с которого идет отгрузка | [optional] 
**items** | [**\Ensi\OmsClient\Dto\OrderCommitRequestItems[]**](OrderCommitRequestItems.md) | Офферы, попавшие в эту отгрузку | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


