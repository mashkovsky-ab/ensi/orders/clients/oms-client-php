# # RefundForCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_id** | **int** | идентификатор заказа | [optional] 
**manager_id** | **int** | идентификатор администратора (если автором был администратор) | [optional] 
**source** | **int** | источник взаимодействия (канал) | [optional] 
**user_comment** | **string** | комментарий пользователя | [optional] 
**status** | **int** | статус заявки на возврат из RefundStatusEnum | [optional] 
**responsible_id** | **int** | идентификатор ответственного | [optional] 
**rejection_comment** | **string** | причина отклонения | [optional] 
**order_items** | [**\Ensi\OmsClient\Dto\RefundOnlyCreateRequestPropertiesOrderItems[]**](RefundOnlyCreateRequestPropertiesOrderItems.md) |  | [optional] 
**refund_reason_ids** | **int[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


