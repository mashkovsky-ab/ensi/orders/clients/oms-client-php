<?php
/**
 * RefundTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * OMS
 *
 * Управление заказами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\OmsClient;

use PHPUnit\Framework\TestCase;

/**
 * RefundTest Class Doc Comment
 *
 * @category    Class
 * @description Refund
 * @package     Ensi\OmsClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class RefundTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Refund"
     */
    public function testRefund()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "price"
     */
    public function testPropertyPrice()
    {
    }

    /**
     * Test attribute "is_partial"
     */
    public function testPropertyIsPartial()
    {
    }

    /**
     * Test attribute "files"
     */
    public function testPropertyFiles()
    {
    }

    /**
     * Test attribute "created_at"
     */
    public function testPropertyCreatedAt()
    {
    }

    /**
     * Test attribute "updated_at"
     */
    public function testPropertyUpdatedAt()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "responsible_id"
     */
    public function testPropertyResponsibleId()
    {
    }

    /**
     * Test attribute "rejection_comment"
     */
    public function testPropertyRejectionComment()
    {
    }

    /**
     * Test attribute "order_id"
     */
    public function testPropertyOrderId()
    {
    }

    /**
     * Test attribute "manager_id"
     */
    public function testPropertyManagerId()
    {
    }

    /**
     * Test attribute "source"
     */
    public function testPropertySource()
    {
    }

    /**
     * Test attribute "user_comment"
     */
    public function testPropertyUserComment()
    {
    }

    /**
     * Test attribute "order"
     */
    public function testPropertyOrder()
    {
    }

    /**
     * Test attribute "items"
     */
    public function testPropertyItems()
    {
    }

    /**
     * Test attribute "reasons"
     */
    public function testPropertyReasons()
    {
    }
}
