# Ensi\OmsClient\ShipmentsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getShipment**](ShipmentsApi.md#getShipment) | **GET** /orders/shipments/{id} | Получение объекта типа Shipment
[**patchShipment**](ShipmentsApi.md#patchShipment) | **PATCH** /orders/shipments/{id} | Обновление части полей объекта типа Shipment
[**searchShipments**](ShipmentsApi.md#searchShipments) | **POST** /orders/shipments:search | Поиск объектов типа Shipment



## getShipment

> \Ensi\OmsClient\Dto\ShipmentResponse getShipment($id, $include)

Получение объекта типа Shipment

Получение объекта типа Shipment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\ShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getShipment($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShipmentsApi->getShipment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\OmsClient\Dto\ShipmentResponse**](../Model/ShipmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchShipment

> \Ensi\OmsClient\Dto\ShipmentResponse patchShipment($id, $shipment_for_patch)

Обновление части полей объекта типа Shipment

Обновление части полей объекта типа Shipment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\ShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$shipment_for_patch = new \Ensi\OmsClient\Dto\ShipmentForPatch(); // \Ensi\OmsClient\Dto\ShipmentForPatch | 

try {
    $result = $apiInstance->patchShipment($id, $shipment_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShipmentsApi->patchShipment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **shipment_for_patch** | [**\Ensi\OmsClient\Dto\ShipmentForPatch**](../Model/ShipmentForPatch.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\ShipmentResponse**](../Model/ShipmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchShipments

> \Ensi\OmsClient\Dto\SearchShipmentsResponse searchShipments($search_shipments_request)

Поиск объектов типа Shipment

Поиск объектов типа Shipment

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\ShipmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_shipments_request = new \Ensi\OmsClient\Dto\SearchShipmentsRequest(); // \Ensi\OmsClient\Dto\SearchShipmentsRequest | 

try {
    $result = $apiInstance->searchShipments($search_shipments_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ShipmentsApi->searchShipments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_shipments_request** | [**\Ensi\OmsClient\Dto\SearchShipmentsRequest**](../Model/SearchShipmentsRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\SearchShipmentsResponse**](../Model/SearchShipmentsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

