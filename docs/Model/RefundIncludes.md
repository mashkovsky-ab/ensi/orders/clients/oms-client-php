# # RefundIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | [**\Ensi\OmsClient\Dto\Order**](Order.md) |  | [optional] 
**items** | [**\Ensi\OmsClient\Dto\OrderItem[]**](OrderItem.md) |  | [optional] 
**reasons** | [**\Ensi\OmsClient\Dto\RefundReason[]**](RefundReason.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


