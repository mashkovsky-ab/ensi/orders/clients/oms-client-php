# # OrderReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID заказа | [optional] 
**number** | **string** | номер заказа | [optional] 
**customer_id** | **int** | id покупателя | [optional] 
**customer_email** | **string** | почта покупателя | [optional] 
**cost** | **int** | стоимость (без учета скидки) (рассчитывается автоматически) в коп. | [optional] 
**price** | **int** | стоимость (с учетом скидок) (рассчитывается автоматически) в коп. | [optional] 
**spent_bonus** | **int** | списано бонусов | [optional] 
**added_bonus** | **int** | начислено бонусов | [optional] 
**promo_code** | **string** | Использованный промокод | [optional] 
**status_at** | [**\DateTime**](\DateTime.md) | дата установки статуса заказа | [optional] 
**source** | **int** | источник заказа из OrderSourceEnum | [optional] 
**payment_status** | **int** | статус оплаты из PaymentStatusEnum | [optional] 
**payment_status_at** | [**\DateTime**](\DateTime.md) | дата установки статуса оплаты | [optional] 
**payed_at** | [**\DateTime**](\DateTime.md) | Дата оплаты | [optional] 
**payment_expires_at** | [**\DateTime**](\DateTime.md) | Дата, до которой нужно провести оплату | [optional] 
**payment_method** | **int** | метод оплаты из PaymentMethodEnum | [optional] 
**payment_system** | **int** | система оплаты из PaymentSystemEnum | [optional] 
**payment_link** | **string** | Ссылка для оплаты во внещней системе | [optional] 
**payment_external_id** | **string** | ID оплаты во внешней системе | [optional] 
**is_problem_at** | [**\DateTime**](\DateTime.md) | дата установки флага проблемного заказа | [optional] 
**is_expired** | **bool** | флаг, что заказ просроченный | [optional] 
**is_expired_at** | [**\DateTime**](\DateTime.md) | дата установки флага просроченного заказа | [optional] 
**is_return** | **bool** | флаг, что заказ возвращен | [optional] 
**is_return_at** | [**\DateTime**](\DateTime.md) | дата установки флага возвращенного заказа | [optional] 
**is_partial_return** | **bool** | флаг, что заказ частично возвращен | [optional] 
**is_partial_return_at** | [**\DateTime**](\DateTime.md) | дата установки флага частично возвращенного заказа | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания заказа | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления заказа | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


