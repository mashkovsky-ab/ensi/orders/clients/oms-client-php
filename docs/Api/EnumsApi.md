# Ensi\OmsClient\EnumsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRefundReason**](EnumsApi.md#createRefundReason) | **POST** /refunds/refund-reasons | Создание объекта типа RefundReason
[**getDeliveryStatuses**](EnumsApi.md#getDeliveryStatuses) | **GET** /orders/delivery-statuses | Получение объекта типа DeliveryStatus
[**getOrderSources**](EnumsApi.md#getOrderSources) | **GET** /orders/order-sources | Получение объекта типа OrderSource
[**getOrderStatuses**](EnumsApi.md#getOrderStatuses) | **GET** /orders/order-statuses | Получение объекта типа OrderStatus
[**getPaymentMethods**](EnumsApi.md#getPaymentMethods) | **GET** /orders/payment-methods | Получение объекта типа PaymentMethod
[**getPaymentStatuses**](EnumsApi.md#getPaymentStatuses) | **GET** /orders/payment-statuses | Получение объекта типа PaymentStatus
[**getPaymentSystems**](EnumsApi.md#getPaymentSystems) | **GET** /payment-systems/payment-systems | Получение объекта типа PaymentSystem
[**getRefundReasons**](EnumsApi.md#getRefundReasons) | **GET** /refunds/refund-reasons | Получение объекта типа RefundReason
[**getRefundStatuses**](EnumsApi.md#getRefundStatuses) | **GET** /refunds/refund-statuses | Получение объекта типа RefundStatus
[**getShipmentStatuses**](EnumsApi.md#getShipmentStatuses) | **GET** /orders/shipment-statuses | Получение объекта типа ShipmentStatus
[**patchRefundReason**](EnumsApi.md#patchRefundReason) | **PATCH** /refunds/refund-reasons/{id} | Обновление части полей объекта типа RefundReason



## createRefundReason

> \Ensi\OmsClient\Dto\RefundReasonResponse createRefundReason($refund_reason_for_create)

Создание объекта типа RefundReason

Создание объекта типа RefundReason

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$refund_reason_for_create = new \Ensi\OmsClient\Dto\RefundReasonForCreate(); // \Ensi\OmsClient\Dto\RefundReasonForCreate | 

try {
    $result = $apiInstance->createRefundReason($refund_reason_for_create);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->createRefundReason: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **refund_reason_for_create** | [**\Ensi\OmsClient\Dto\RefundReasonForCreate**](../Model/RefundReasonForCreate.md)|  | [optional]

### Return type

[**\Ensi\OmsClient\Dto\RefundReasonResponse**](../Model/RefundReasonResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getDeliveryStatuses

> \Ensi\OmsClient\Dto\DeliveryStatusesResponse getDeliveryStatuses()

Получение объекта типа DeliveryStatus

Получение объекта типа DeliveryStatus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getDeliveryStatuses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getDeliveryStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\DeliveryStatusesResponse**](../Model/DeliveryStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getOrderSources

> \Ensi\OmsClient\Dto\OrderSourcesResponse getOrderSources()

Получение объекта типа OrderSource

Получение объекта типа OrderSource

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getOrderSources();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getOrderSources: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\OrderSourcesResponse**](../Model/OrderSourcesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getOrderStatuses

> \Ensi\OmsClient\Dto\OrderStatusesResponse getOrderStatuses()

Получение объекта типа OrderStatus

Получение объекта типа OrderStatus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getOrderStatuses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getOrderStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\OrderStatusesResponse**](../Model/OrderStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPaymentMethods

> \Ensi\OmsClient\Dto\PaymentMethodsResponse getPaymentMethods()

Получение объекта типа PaymentMethod

Получение объекта типа PaymentMethod

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPaymentMethods();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getPaymentMethods: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\PaymentMethodsResponse**](../Model/PaymentMethodsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPaymentStatuses

> \Ensi\OmsClient\Dto\PaymentStatusesResponse getPaymentStatuses()

Получение объекта типа PaymentStatus

Получение объекта типа PaymentStatus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPaymentStatuses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getPaymentStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\PaymentStatusesResponse**](../Model/PaymentStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPaymentSystems

> \Ensi\OmsClient\Dto\PaymentSystemsResponse getPaymentSystems()

Получение объекта типа PaymentSystem

Получение объекта типа PaymentSystem

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPaymentSystems();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getPaymentSystems: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\PaymentSystemsResponse**](../Model/PaymentSystemsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getRefundReasons

> \Ensi\OmsClient\Dto\RefundReasonsResponse getRefundReasons()

Получение объекта типа RefundReason

Получение объекта типа RefundReason

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getRefundReasons();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getRefundReasons: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\RefundReasonsResponse**](../Model/RefundReasonsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getRefundStatuses

> \Ensi\OmsClient\Dto\RefundStatusesResponse getRefundStatuses()

Получение объекта типа RefundStatus

Получение объекта типа RefundStatus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getRefundStatuses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getRefundStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\RefundStatusesResponse**](../Model/RefundStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getShipmentStatuses

> \Ensi\OmsClient\Dto\ShipmentStatusesResponse getShipmentStatuses()

Получение объекта типа ShipmentStatus

Получение объекта типа ShipmentStatus

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getShipmentStatuses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getShipmentStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\ShipmentStatusesResponse**](../Model/ShipmentStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchRefundReason

> \Ensi\OmsClient\Dto\RefundReasonResponse patchRefundReason($id, $refund_reason_for_patch)

Обновление части полей объекта типа RefundReason

Обновление части полей объекта типа RefundReason

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$refund_reason_for_patch = new \Ensi\OmsClient\Dto\RefundReasonForPatch(); // \Ensi\OmsClient\Dto\RefundReasonForPatch | 

try {
    $result = $apiInstance->patchRefundReason($id, $refund_reason_for_patch);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->patchRefundReason: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **refund_reason_for_patch** | [**\Ensi\OmsClient\Dto\RefundReasonForPatch**](../Model/RefundReasonForPatch.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\RefundReasonResponse**](../Model/RefundReasonResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

