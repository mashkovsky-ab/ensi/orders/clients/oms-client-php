# # OrderFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**responsible_id** | **int** | Идентификатор ответственного за заказ | [optional] 
**status** | **int** | статус заказа из OrderStatus | [optional] 
**client_comment** | **string** | комментарий клиента | [optional] 
**receiver_name** | **string** | имя получателя | [optional] 
**receiver_phone** | **string** | телефон получателя | [optional] 
**receiver_email** | **string** | e-mail получателя | [optional] 
**is_problem** | **bool** | флаг, что заказ проблемный | [optional] 
**problem_comment** | **string** | последнее сообщение продавца о проблеме со сборкой | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


