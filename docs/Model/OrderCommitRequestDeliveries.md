# # OrderCommitRequestDeliveries

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**\DateTime**](\DateTime.md) | Выбранная дата доставки | [optional] 
**timeslot** | [**\Ensi\OmsClient\Dto\Timeslot**](Timeslot.md) |  | [optional] 
**cost** | **int** | Итоговая стоимость доставки (в копейках) | [optional] 
**shipments** | [**\Ensi\OmsClient\Dto\OrderCommitRequestShipments[]**](OrderCommitRequestShipments.md) | Отгрузки, входящие в отправление | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


